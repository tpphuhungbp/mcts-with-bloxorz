import pygame, sys, re, time, gridManager, buttonObject
from queue import Queue
from pygame.locals import *
from enum import Enum, unique, Flag

## Hàm move 
## Nhận tọa độ hiện tại và các kí tự ‘L’, ‘R’, ‘U’, ‘D’
## Xuất ra tọa độ tiếp theo và biến res ( res = -1: thua, res =0: bình thường, res=1:thắng)

def move(curx1,cury1,curx2,cury2,char, softButtonStatusString, hardButtonStatusString, playerType):
    x1 = curx1
    y1 = cury1
    x2 = curx2
    y2 = cury2
    res = 0
    if char == 'L':
        if playerType == 0:
            if y1 == y2 and x1 == x2:
                x1-=2
                x2-=1
            elif y1 == y2:
                x1 = x2 = min(x1,x2)-1
            elif x1 == x2:
                x1-=1
                x2-=1
        elif playerType == 1: 
            x1-=1
        else: 
            x2-=1
    elif char == 'R':
        if playerType == 0:
            if y1 == y2 and x1 == x2:
                x1+=2
                x2+=1
            elif y1 == y2:
                x1 = x2 = min(x1,x2)+2
            elif x1 == x2:
                x1+=1
                x2+=1
        elif playerType == 1:
            x1+=1
        else:
            x2+=1
    elif char == 'U':
        if playerType == 0:
            if x1 == x2 and y1 == y2:
                y1-=2
                y2-=1
            elif x1 == x2:
                y1 = y2 = min(y1,y2)-1
            elif y1 == y2:
                y1-=1
                y2-=1
        elif playerType == 1:
            y1-=1
        else:
            y2-=1
    elif char == 'D':
        if playerType == 0:
            if x1 == x2 and y1 == y2:
                y1+=2
                y2+=1
            elif x1 == x2:
                y1 = y2 = min(y1,y2)+2
            elif y1 == y2:
                y1+=1
                y2+=1    
        elif playerType == 1:
            y1+=1
        else:
            y2+=1
    elif char == 'S':
        if playerType == 0:
            #Khong the switch player khi dang o trang thai dinh nhau!
            res = -1
        else:
            playerType = playerType % 2 + 1 # toggle gia tri 1 va 2 cua player.type

    if char != 'S':
        if (x1<0 or x1>=gridManager.initalGrid.width or y1<0 or y1>=gridManager.initalGrid.height or
            x2<0 or x2>=gridManager.initalGrid.width or y2<0 or y2>=gridManager.initalGrid.height):
            res = -1
        else:
            if gridManager.initalGrid.arr[y1][x1] == 4 or gridManager.initalGrid.arr[y2][x2] == 4:
                for i in range(0,len(buttonObject.softSwitchList)):
                    if buttonObject.softSwitchList[i].checkPlayerOn(x1, y1, x2, y2) == True:
                        if (softButtonStatusString[i] == '0'):
                            if buttonObject.softSwitchList[i].type != 2:
                                for index in buttonObject.softSwitchList[i].softSameIndexList:
                                    st = list(softButtonStatusString)
                                    st[index] = '1'
                                    softButtonStatusString = "".join(st)
                                for index in buttonObject.softSwitchList[i].hardSameIndexList:
                                    st = list(hardButtonStatusString)
                                    st[index] = '1'
                                    hardButtonStatusString = "".join(st)
                        else:
                            if buttonObject.softSwitchList[i].type != 1:
                                for index in buttonObject.softSwitchList[i].softSameIndexList:
                                    st = list(softButtonStatusString)
                                    st[index] = '0'
                                    softButtonStatusString = "".join(st)
                                for index in buttonObject.softSwitchList[i].hardSameIndexList:
                                    st = list(hardButtonStatusString)
                                    st[index] = '0'
                                    hardButtonStatusString = "".join(st)
            if gridManager.initalGrid.arr[y1][x1] == 5 or gridManager.initalGrid.arr[y2][x2] == 5:
                for i in range(0,len(buttonObject.hardSwitchList)):
                    if buttonObject.hardSwitchList[i].checkPlayerOn(x1, y1, x2, y2) == True:
                        if (hardButtonStatusString[i] == '0'):
                            if buttonObject.hardSwitchList[i].type != 2:
                                for index in buttonObject.hardSwitchList[i].softSameIndexList:
                                    st = list(softButtonStatusString)
                                    st[index] = '1'
                                    softButtonStatusString = "".join(st)
                                for index in buttonObject.hardSwitchList[i].hardSameIndexList:
                                    st = list(hardButtonStatusString)
                                    st[index] = '1'
                                    hardButtonStatusString = "".join(st)
                        else:
                            if buttonObject.hardSwitchList[i].type != 1:
                                for index in buttonObject.hardSwitchList[i].softSameIndexList:
                                    st = list(softButtonStatusString)
                                    st[index] = '0'
                                    softButtonStatusString = "".join(st)
                                for index in buttonObject.hardSwitchList[i].hardSameIndexList:
                                    st = list(hardButtonStatusString)
                                    st[index] = '0'
                                    hardButtonStatusString = "".join(st)

            if gridManager.initalGrid.arr[y1][x1] == 6 or gridManager.initalGrid.arr[y2][x2] == 6:
                for i in range(0,len(buttonObject.splitSwitchList)):
                    if buttonObject.splitSwitchList[i].checkPlayerOn(x1, y1, x2, y2) == True:
                        playerType = 1
                        x1, y1 = buttonObject.splitSwitchList[i].pos1
                        x2, y2 = buttonObject.splitSwitchList[i].pos2

            isOnBridgeSoft1 = False
            isOnBridgeHard1 = False
            isOnBridgeSoft2 = False
            isOnBridgeHard2 = False
            for i in range(0,len(buttonObject.softSwitchList)):
                button = buttonObject.softSwitchList[i]
                for j in range(0,len(button.bridgeXList)):
                    if (button.bridgeXList[j] == x1 and button.bridgeYList[j] == y1):
                        isOnBridgeSoft1 = True
                        if softButtonStatusString[i] == '0':
                            res = -1      
                    if (button.bridgeXList[j] == x2 and button.bridgeYList[j] == y2):
                        isOnBridgeSoft2 = True
                        if softButtonStatusString[i] == '0':
                            res = -1
            for i in range(0,len(buttonObject.hardSwitchList)):
                button = buttonObject.hardSwitchList[i]
                for j in range(0,len(button.bridgeXList)):                 
                    if (button.bridgeXList[j] == x1 and button.bridgeYList[j] == y1):
                        isOnBridgeHard1 = True
                        if hardButtonStatusString[i] == '0':
                            res = -1
                    if (button.bridgeXList[j] == x2 and button.bridgeYList[j] == y2):
                        isOnBridgeHard2 = True
                        if hardButtonStatusString[i] == '0':
                            res = -1

            
            if isOnBridgeHard1 == False and isOnBridgeSoft1==False and gridManager.initalGrid.arr[y1][x1] == 0:
                res = -1
            if isOnBridgeHard2 == False and isOnBridgeSoft2==False and gridManager.initalGrid.arr[y2][x2] == 0:
                res = -1

            if gridManager.initalGrid.arr[y1][x1] == 2 and gridManager.initalGrid.arr[y2][x2] == 2:
                res = 1
            if gridManager.initalGrid.arr[y1][x1] == 3 and x1 == x2 and y1 == y2:
                res = -1
            if abs(x1-x2) + abs(y1-y2) <= 1:
                playerType = 0

    return x1,y1,x2,y2,res, softButtonStatusString, hardButtonStatusString, playerType