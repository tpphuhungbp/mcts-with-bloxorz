import pygame
import sys
import re
import time
import os
import uiManager
import gridManager
import setting
import playerManager
import buttonObject
import os.path
import psutil
import tracemalloc
from Solve import bfs, astar, mcts
from queue import Queue
from pygame.locals import *
from enum import Enum, unique, Flag

pygame.init()

### Xác định FPS ###
FPS = 60
fpsClock = pygame.time.Clock()


font = pygame.font.SysFont('consolas', 18)
textWinSurface = font.render('YOU WIN!', True, setting.GREEN)
textLoseSurface = font.render('YOU LOSE!', True, setting.RED)

# ---------------------------------------function----------------------


def moveLeft():
    player.isMoveLeft = True


def moveRight():
    player.isMoveRight = True


def moveUp():
    player.isMoveUp = True


def moveDown():
    player.isMoveDown = True


def loadData(levelPath):
    global isCanLoadLevel, initalSoftButtonStatusString, initalHardButtonStatusString
    x1 = 0
    y1 = 0
    x2 = 0
    y2 = 0
    width = 0
    height = 0

    arr = []

    if os.path.isfile(levelPath) == True:
        isCanLoadLevel = True
        f = open(levelPath)
    else:
        isCanLoadLevel = False
        return

    x1, y1, x2, y2 = [int(x) for x in next(f).split()]  # Doc dong dau tien
    width, height = [int(x) for x in next(f).split()]  # Doc dong thu 2
    softSwitchCount, hardSwitchCount, splitSwitchCount = [
        int(x) for x in next(f).split()]  # Doc dong thu 3

    buttonObject.softSwitchList = list()
    for i in range(0, softSwitchCount):
        line = next(f).split()
        xPos = 0
        yPos = 0
        switchType = 0

        count = 0
        for x in line:
            if count == 0:
                switchType = int(x)
            elif count == 1:
                xPos = int(x)
            elif count == 2:
                yPos = int(x)
                softSwitch = buttonObject.SoftSwitch(xPos, yPos, switchType)
                buttonObject.softSwitchList.append(softSwitch)
            else:
                if count % 2 == 1:
                    buttonObject.softSwitchList[-1].bridgeXList.append(int(x))
                else:
                    buttonObject.softSwitchList[-1].bridgeYList.append(int(x))
            count += 1

    buttonObject.hardSwitchList = list()
    for i in range(0, hardSwitchCount):
        line = next(f).split()
        xPos = 0
        yPos = 0
        switchType = 0

        count = 0
        for x in line:
            if count == 0:
                switchType = int(x)
            elif count == 1:
                xPos = int(x)
            elif count == 2:
                yPos = int(x)
                hardSwitch = buttonObject.HardSwitch(xPos, yPos, switchType)
                buttonObject.hardSwitchList.append(hardSwitch)
            else:
                if count % 2 == 1:
                    buttonObject.hardSwitchList[-1].bridgeXList.append(int(x))
                else:
                    buttonObject.hardSwitchList[-1].bridgeYList.append(int(x))
            count += 1

    buttonObject.splitSwitchList = list()
    for i in range(0, splitSwitchCount):
        x, y, s_x1, s_y1, s_x2, s_y2 = [int(x) for x in next(f).split()]
        splitSwitch = buttonObject.SplitSwitch(x, y, s_x1, s_y1, s_x2, s_y2)
        buttonObject.splitSwitchList.append(splitSwitch)

    # Check nhung button trung vi tri cau voi nhau
    for button in buttonObject.softSwitchList:
        for i in range(0, len(buttonObject.softSwitchList)):
            if button.bridgeXList == buttonObject.softSwitchList[i].bridgeXList and button.bridgeYList == buttonObject.softSwitchList[i].bridgeYList:
                button.softSameIndexList.append(i)
        for i in range(0, len(buttonObject.hardSwitchList)):
            if button.bridgeXList == buttonObject.hardSwitchList[i].bridgeXList and button.bridgeYList == buttonObject.hardSwitchList[i].bridgeYList:
                button.hardSameIndexList.append(i)
    for button in buttonObject.hardSwitchList:
        for i in range(0, len(buttonObject.softSwitchList)):
            if button.bridgeXList == buttonObject.softSwitchList[i].bridgeXList and button.bridgeYList == buttonObject.softSwitchList[i].bridgeYList:
                button.softSameIndexList.append(i)
        for i in range(0, len(buttonObject.hardSwitchList)):
            if button.bridgeXList == buttonObject.hardSwitchList[i].bridgeXList and button.bridgeYList == buttonObject.hardSwitchList[i].bridgeYList:
                button.hardSameIndexList.append(i)
    # print(buttonObject.softSwitchList[3].softSameIndexList)

    for line in f:  # Doc cac dong con lai trong file
        numbers = [int(x) for x in line.split()]
        arr.append(numbers)

    gridManager.grid.setData(width, height, arr)
    gridManager.initalGrid.setData(width, height, arr)

    initalSoftButtonStatusString = ""
    initalHardButtonStatusString = ""
    for button in buttonObject.softSwitchList:
        if gridManager.initalGrid.arr[button.bridgeYList[0]][button.bridgeXList[0]] == 0:
            initalSoftButtonStatusString += "0"
        else:
            initalSoftButtonStatusString += "1"
    for button in buttonObject.hardSwitchList:
        if gridManager.initalGrid.arr[button.bridgeYList[0]][button.bridgeXList[0]] == 0:
            initalHardButtonStatusString += "0"
        else:
            initalHardButtonStatusString += "1"

    player.setData(x1, y1, x2, y2)


def checkWinLose():
    player.isEndMoving = False
    if (player.x1Index < 0 or player.x1Index >= gridManager.grid.width or
        player.y1Index < 0 or player.y1Index >= gridManager.grid.height or
        player.x2Index < 0 or player.x2Index >= gridManager.grid.width or
            player.y2Index < 0 or player.y2Index >= gridManager.grid.height):
        announceWinLose(False)
        return
    if gridManager.grid.arr[player.y1Index][player.x1Index] == 0 or gridManager.grid.arr[player.y2Index][player.x2Index] == 0:
        announceWinLose(False)
        return
    if gridManager.grid.arr[player.y1Index][player.x1Index] == 3 and player.x1Index == player.x2Index and player.y1Index == player.y2Index:
        announceWinLose(False)
        return
    if gridManager.grid.arr[player.y1Index][player.x1Index] == 2 and gridManager.grid.arr[player.y2Index][player.x2Index] == 2:
        announceWinLose(True)


def announceWinLose(isWin):
    global isEndGame
    if isWin:
        setting.DISPLAYSURF.blit(textWinSurface, (0, 80))
    else:
        setting.DISPLAYSURF.blit(textLoseSurface, (0, 80))
    isEndGame = True


def displayText():
    global isCanLoadLevel, levelPath, timeAlgorithm, memo_info
    textSurface = font.render(
        '(X1,Y1) = (' + str(player.x1Index) + ',' + str(player.y1Index) + ')', True, setting.RED)
    setting.DISPLAYSURF.blit(textSurface, (0, 0))

    textSurface = font.render(
        '(X2,Y2) = (' + str(player.x2Index) + ',' + str(player.y2Index) + ')', True, setting.GREEN)
    setting.DISPLAYSURF.blit(textSurface, (0, 20))

    textSurface = font.render(
        'Count: ' + str(player.count), True, setting.BLACK)
    setting.DISPLAYSURF.blit(textSurface, (0, 40))

    if isCanLoadLevel == True:
        textSurface = font.render(
            'Load "' + levelPath + '" successed!', True, setting.GREEN)
        setting.DISPLAYSURF.blit(textSurface, (0, 60))
    else:
        textSurface = font.render(
            'Load "' + levelPath + '" failed!', True, setting.RED)
        setting.DISPLAYSURF.blit(textSurface, (0, 60))

    textSurface = font.render(
        'Solving time: ' + str(round(timeAlgorithm, 4)) + "s", True, setting.BLACK)
    setting.DISPLAYSURF.blit(textSurface, (350, 0))
    textSurface = font.render(
        'Memory used: ' + str(round(memo_info, 4)) + "MB", True, setting.BLACK)
    setting.DISPLAYSURF.blit(textSurface, (350, 20))


def BlindSearch():
    global initalSoftButtonStatusString, initalHardButtonStatusString
    startTime = time.time()
    tracemalloc.start()
    st = bfs.bfs(player.x1Index, player.y1Index, player.x2Index, player.y2Index,
                 initalSoftButtonStatusString, initalHardButtonStatusString, 0)
    current, _ = tracemalloc.get_traced_memory()
    memo_info = current/(1024*1024)
    tracemalloc.stop()
    endTime = time.time()
    return st, endTime-startTime, memo_info


def AStar():
    global initalSoftButtonStatusString, initalHardButtonStatusString
    startTime = time.time()
    tracemalloc.start()

    initialPos = astar.Position(player.x1Index, player.y1Index, player.x2Index,
                                 player.y2Index, initalSoftButtonStatusString, initalHardButtonStatusString, 0)
    st = astar.aStarSearch(initialPos, gridManager.grid.goalX, gridManager.grid.goalY,
                             initalSoftButtonStatusString, initalHardButtonStatusString, 0)

    current, _ = tracemalloc.get_traced_memory()
    memo_info = current/(1024*1024)
    tracemalloc.stop()
    endTime = time.time()
    return st, endTime-startTime, memo_info


def MCTS():
    global initalSoftButtonStatusString, initalHardButtonStatusString, maxIter, numSimulate
    startTime = time.time()
    tracemalloc.start()
    mtcsTree = mcts.MonteCarloTreeSearch(player.x1Index, player.y1Index, player.x2Index,
                                         player.y2Index, initalSoftButtonStatusString, initalHardButtonStatusString, 0)
    st = mcts.mctsSolver(mtcsTree, maxIter, numSimulate)
    current, _ = tracemalloc.get_traced_memory()
    memo_info = current/(1024*1024)
    tracemalloc.stop()
    endTime = time.time()
    return st, endTime-startTime, memo_info


def reset(levelPath, isDoAlgorthm):
    global isEndGame, isStartGame, player, st, selectButton, timeAlgorithm, memo_info
    player.isCanMove = True
    player.isMoveLeft = False
    player.isMoveRight = False
    player.isMoveUp = False
    player.isMoveDown = False

    isStartGame = False
    isEndGame = False
    player.count = 0

    loadData(levelPath)

    if isDoAlgorthm == True:
        textSurface = font.render(
            'LOADING...', True, setting.BLACK)
        setting.DISPLAYSURF.blit(textSurface, (1050, 70))
        pygame.display.update()
        if selectButton.activeList[0] == True:  # blind search
            st, timeAlgorithm, memo_info = BlindSearch()
        elif selectButton.activeList[1] == True:  # A*
            st, timeAlgorithm, memo_info = AStar()
        elif selectButton.activeList[2] == True:  # MCTS
            st, timeAlgorithm, memo_info = MCTS()

    # loadData(levelPath) #Load lai lan 2 de tranh cac button object bi kich hoat sau khi da thuc hien thuat toan
    setting.DISPLAYSURF.fill(setting.WHITE)

    gridManager.grid.draw()
    startButton.draw()
    selectButton.draw()
    levelInput.draw()
    if selectButton.activeList[2] == True:
        maxIterInput.draw()
        numSimulateInput.draw()
    player.draw()

    displayText()

    pygame.display.update()
    fpsClock.tick(FPS)


def getInputEvent():
    global levelPath, isPerforming, isCanLoadLevel, maxIter, numSimulate
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.MOUSEBUTTONDOWN:
            if startButton.checkOnMouseOver() == True and isCanLoadLevel == True:
                reset(levelPath, False)
                isPerforming = True
                performMoving(st)
                return
            levelInput.input_active = False
            maxIterInput.input_active = False
            numSimulateInput.input_active = False
            if levelInput.button.checkOnMouseOver() == True:
                levelInput.input_active = True
                levelInput.text = ""
            if selectButton.activeList[2] == True:
                if maxIterInput.button.checkOnMouseOver() == True:
                    maxIterInput.input_active = True
                    maxIterInput.text = ""
                if numSimulateInput.button.checkOnMouseOver() == True:
                    numSimulateInput.input_active = True
                    numSimulateInput.text = ""

            if selectButton.getButtonMouseOver() != -1:
                selectButton.chooseButton()
                reset(levelPath, True)
                isPerforming = False
        if event.type == pygame.KEYDOWN:
            if levelInput.input_active == True:
                if event.key == pygame.K_RETURN:
                    levelPath = "Level/" + levelInput.text + ".txt"
                    if maxIterInput.text == "":
                        maxIter = 0
                    else:
                        maxIter = int(maxIterInput.text)
                    if numSimulateInput.text == "":
                        numSimulateIter = 0
                    else:
                        numSimulate = int(numSimulateInput.text)
                    levelInput.input_active = False
                    reset(levelPath, True)
                    isPerforming = False
                elif event.key == pygame.K_BACKSPACE:
                    levelInput.text = levelInput.text[:-1]
                else:
                    if len(levelInput.text) < 3:  # So ki tu nhap toi da la 3
                        levelInput.text += event.unicode
            if maxIterInput.input_active == True:
                if event.key == pygame.K_RETURN:
                    levelPath = "Level/" + levelInput.text + ".txt"
                    if maxIterInput.text == "":
                        maxIter = 0
                    else:
                        maxIter = int(maxIterInput.text)
                    if numSimulateInput.text == "":
                        numSimulateIter = 0
                    else:
                        numSimulate = int(numSimulateInput.text)
                    maxIterInput.input_active = False
                    reset(levelPath, True)
                    isPerforming = False
                elif event.key == pygame.K_BACKSPACE:
                    maxIterInput.text = maxIterInput.text[:-1]
                else:
                    if len(maxIterInput.text) < 6 and '0'<=event.unicode<='9':  # So ki tu nhap toi da la 6
                        maxIterInput.text += event.unicode
            if numSimulateInput.input_active == True:
                if event.key == pygame.K_RETURN:
                    levelPath = "Level/" + levelInput.text + ".txt"
                    if maxIterInput.text == "":
                        maxIter = 0
                    else:
                        maxIter = int(maxIterInput.text)
                    if numSimulateInput.text == "":
                        numSimulateIter = 0
                    else:
                        numSimulate = int(numSimulateInput.text)
                    numSimulateInput.input_active = False
                    reset(levelPath, True)
                    isPerforming = False
                elif event.key == pygame.K_BACKSPACE:
                    numSimulateInput.text = numSimulateInput.text[:-1]
                else:
                    if len(numSimulateInput.text) < 6 and '0'<=event.unicode<='9':  # So ki tu nhap toi da la 6
                        numSimulateInput.text += event.unicode


def performMoving(st):
    global level, isPerforming
    i = 0

    if st == "":
        textSurface = font.render('NO SOLUTIONS FOUND!!!', True, setting.RED)
        setting.DISPLAYSURF.blit(textSurface, (350, 40))
        pygame.display.update()
        fpsClock.tick(FPS)
        return

    while i < len(st):
        if st[i] == 'U':
            moveUp()
        elif st[i] == 'D':
            moveDown()
        elif st[i] == 'L':
            moveLeft()
        elif st[i] == 'R':
            moveRight()
        elif st[i] == 'S':
            if player.type == 0:
                print("Khong the switch player khi dang o trang thai dinh nhau!")
            else:
                player.type = player.type % 2 + 1  # toggle gia tri 1 va 2 cua player.type
        else:
            print(st[i] + 'KHONG NAM TRONG KI TU DI CHUYEN')
        player.update()
        i += 1

        getInputEvent()
        if isPerforming == False:
            return

        while player.isCanMove == False:

            getInputEvent()
            if isPerforming == False:
                return

            if isEndGame == False:
                setting.DISPLAYSURF.fill(setting.WHITE)

                gridManager.grid.draw()
                startButton.draw()
                player.update()
                player.draw()
                levelInput.draw()
                if selectButton.activeList[2] == True:
                    maxIterInput.draw()
                    numSimulateInput.draw()

                displayText()

                if (i < len(st)):
                    # textSurface = font.render('(PRESS SPACE TO MOVE)', True, setting.BLACK)
                    pass
                else:
                    textSurface = font.render(
                        '(THE MOVING STRING IS END!)', True, setting.BLACK)
                    setting.DISPLAYSURF.blit(textSurface, (350, 40))

            startButton.draw()
            selectButton.draw()
            levelInput.draw()
            if selectButton.activeList[2] == True:
                maxIterInput.draw()
                numSimulateInput.draw()
            pygame.display.update()
            fpsClock.tick(FPS)

        if gridManager.grid.arr[player.y1Index][player.x1Index] == 4 or gridManager.grid.arr[player.y2Index][player.x2Index] == 4:
            for button in buttonObject.softSwitchList:
                if button.checkPlayerOn(player.x1Index, player.y1Index, player.x2Index, player.y2Index) == True:
                    button.setBridge()
        if gridManager.grid.arr[player.y1Index][player.x1Index] == 5 or gridManager.grid.arr[player.y2Index][player.x2Index] == 5:
            for button in buttonObject.hardSwitchList:
                if button.checkPlayerOn(player.x1Index, player.y1Index, player.x2Index, player.y2Index) == True:
                    button.setBridge()
        if gridManager.grid.arr[player.y1Index][player.x1Index] == 6 or gridManager.grid.arr[player.y2Index][player.x2Index] == 6:
            for button in buttonObject.splitSwitchList:
                if button.checkPlayerOn(player.x1Index, player.y1Index, player.x2Index, player.y2Index) == True:
                    player.split(button.pos1, button.pos2)

        if player.isEndMoving == True:
            checkWinLose()

        startButton.draw()
        selectButton.draw()
        gridManager.grid.draw()
        player.draw()
        pygame.display.update()
        fpsClock.tick(FPS)

        time.sleep(0.3)


# -------------------------------------__MAIN___-----------------------------------
player = playerManager.Player()
startButton = uiManager.Button(
    1050, 0, 100, 50, "Sprite/StartButtonIdle.png", "Sprite/StartButtonHover.png", "")
blindSearchButton = uiManager.Button(885, 0, 150, 30, "Sprite/BlindSearchButtonIdle.png",
                                     "Sprite/BlindSearchButtonHover.png", "Sprite/BlindSearchButtonDown.png")
AStarButton = uiManager.Button(885, 35, 150, 30, "Sprite/AStarButtonIdle.png",
                               "Sprite/AStarButtonHover.png", "Sprite/AStarButtonDown.png")
MCTSButton = uiManager.Button(885, 70, 150, 30, "Sprite/MCTSButtonIdle.png",
                              "Sprite/MCTSButtonHover.png", "Sprite/MCTSButtonDown.png")
selectButton = uiManager.SelectButton(
    [blindSearchButton, AStarButton, MCTSButton])
levelInput = uiManager.InputField(
    790, 0, 80, 30, "       Level", "Sprite/LevelInputFieldIdle.png", "Sprite/LevelInputFieldHover.png", "")
maxIterInput = uiManager.InputField(
    790, 35, 80, 30, "     MaxIter", "Sprite/LevelInputFieldIdle.png", "Sprite/LevelInputFieldHover.png", "")
numSimulateInput = uiManager.InputField(
    790, 70, 80, 30, "Num simulate", "Sprite/LevelInputFieldIdle.png", "Sprite/LevelInputFieldHover.png", "")

maxIter = 0
numSimulate = 0
# grid = Grid()
player.isCanMove = True
player.isMoveLeft = False
player.isMoveRight = False
player.isMoveUp = False
player.isMoveDown = False

isCanLoadLevel = True
isPerforming = False
isEndGame = False
player.count = 0

levelPath = 'Level/1.txt'
reset(levelPath, True)

# st = "DRRRULLLDRRULDR"

while True:
    if player.isEndMoving == True:
        checkWinLose()

    getInputEvent()

    startButton.draw()
    selectButton.draw()
    levelInput.draw()
    if selectButton.activeList[2] == True:
        maxIterInput.draw()
        numSimulateInput.draw()
    pygame.display.update()
    fpsClock.tick(FPS)
