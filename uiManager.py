import pygame, sys, re, time, os, setting
import os.path
from Solve import bfs
from queue import Queue
from pygame.locals import *
from enum import Enum, unique, Flag

####---------------------------------------UI class-----------------------
class Button():
    def __init__(self, x, y, width, height, idleStr, hoverStr, downStr):
        self.x = x
        self.y = y
        self.width = width
        self.height = height

        self.surfaceIdle = pygame.image.load(idleStr)
        self.surfaceHover = pygame.image.load(hoverStr)
        if downStr != "":
           self.surfaceDown = pygame.image.load(downStr)
        
    def draw(self):
        if self.checkOnMouseOver() == False:
            setting.DISPLAYSURF.blit(self.surfaceIdle, (self.x, self.y))
        else:
            setting.DISPLAYSURF.blit(self.surfaceHover, (self.x, self.y))
    def drawChoosing(self):
        setting.DISPLAYSURF.blit(self.surfaceDown, (self.x, self.y))

    def checkOnMouseOver(self):
        mouse = pygame.mouse.get_pos()
        if self.x <= mouse[0] <= self.x+self.width and self.y <= mouse[1] <= self.y+self.height:
            return True
        return False

class SelectButton():
    def __init__(self, buttonList):
        self.buttonList = buttonList
        self.activeList = list();
        for i in range(0,len(self.buttonList)):
            self.activeList.append(False)
        self.activeList[0] = True
    def draw(self):
        for i in range(0,len(self.buttonList)):
            if self.activeList[i] == False:
                self.buttonList[i].draw()
            else:
                self.buttonList[i].drawChoosing()
    def chooseButton(self):
        for i in range(0,len(self.buttonList)):
            self.activeList[i] = False
        self.activeList[self.getButtonMouseOver()] = True
    def getButtonMouseOver(self):
        for i in range(0,len(self.buttonList)):
            if self.buttonList[i].checkOnMouseOver() == True:
                return i
        return -1

class InputField():
    def __init__(self, x, y, width, height, inputName, idleStr, hoverStr, downStr):
        self.button = Button(x, y, width, height, idleStr, hoverStr, downStr)
        self.textX = x + 10
        self.textY = y + 5
        self.inputName = inputName + ":"
        self.input_active = False
        self.text = ""
    def draw(self):
        self.button.draw()

        font = pygame.font.SysFont('consolas', 18)
        self.textSurface = font.render(self.inputName, True, setting.BLACK)
        setting.DISPLAYSURF.blit(self.textSurface, (self.button.x-140, self.textY))

        self.textSurface = font.render(self.text, True, setting.BLACK)
        setting.DISPLAYSURF.blit(self.textSurface, (self.textX, self.textY))