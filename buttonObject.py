import pygame, sys, re, time, setting, gridManager
from queue import Queue
from pygame.locals import *
from enum import Enum, unique, Flag

class Switch():
    def __init__(self, x, y, type):
        self.bridgeXList = list()
        self.bridgeYList = list()
        self.x = x
        self.y = y
        self.type = type
        self.softSameIndexList = list()
        self.hardSameIndexList = list()
    def setBridge(self):
        for i in range(0,len(self.bridgeXList)):
            x,y = self.bridgeXList[i],self.bridgeYList[i]
            if self.type == 0:
                gridManager.grid.arr[y][x] = (gridManager.grid.arr[y][x] + 1)%2 #toggle 0 1 gia tri Grid.grid.arr[x][y]
            elif self.type == 1:
                gridManager.grid.arr[y][x] = 1
            else:
                gridManager.grid.arr[y][x] = 0

class SoftSwitch(Switch):
    def __init__(self, x, y, type):
        super().__init__(x,y, type)
    def checkPlayerOn(self,x1,y1,x2,y2):
        if (x1==self.x and y1==self.y) or (x2==self.x and y2==self.y):
            return True
        return False

class HardSwitch(Switch):
    def __init__(self, x, y, type):
        super().__init__(x,y, type)
    def checkPlayerOn(self,x1,y1,x2,y2):
        if x1==self.x and y1==self.y and x2==self.x and y2==self.y:
            return True
        return False

class SplitSwitch():
    def __init__(self, x, y, x1, y1, x2, y2):
        self.x = x
        self.y = y
        self.pos1 = x1, y1
        self.pos2 = x2, y2
    def checkPlayerOn(self,x1,y1,x2,y2):
        if x1==self.x and y1==self.y and x2==self.x and y2==self.y:
            return True
        return False

softSwitchList = list()
hardSwitchList = list()
splitSwitchList = list()