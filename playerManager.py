import pygame, sys, re, time, setting, Grid
from queue import Queue
from pygame.locals import *
from enum import Enum, unique, Flag

class Player():
    
    def __init__(self):
        self.x1Index = 0
        self.y1Index = 0
        self.x2Index = 0
        self.y2Index = 0

        self.count = 0;
        self.type = 0; # 0: hai khối dính nhau, 1: khối 1 di chuyển riêng, 2: khối 2 di chuyển riêng

        self.playerStatus = setting.STATUS.NOT_VALID

        self.x1 = setting.INIT_X + self.x1Index*setting.STEP
        self.y1 = setting.INIT_Y + self.y1Index*setting.STEP
        self.x2 = setting.INIT_X + self.x2Index*setting.STEP
        self.y2 = setting.INIT_Y + self.y2Index*setting.STEP

        self.isDraw1First = True
        self.isCanMove = True
        self.isMoveLeft = False
        self.isMoveRight = False
        self.isMoveUp = False
        self.isMoveDown = False
        self.isEndMoving = False
        
        ## Tạo surface và thêm hình chiếc xe vào ##
        #self.surface = pygame.image.load('car.png')
        self.surface1 = pygame.image.load("Sprite/player1.png")
        #pygame.draw.rect(self.surface1, RED, (0, 0, 50, 50))

        self.surface2 = pygame.image.load("Sprite/player2.png")
        #pygame.draw.rect(self.surface2, GREEN, (0, 0, 50, 50))

    def setData(self, x1Index, y1Index, x2Index, y2Index):
        self.x1Index = int(x1Index)
        self.y1Index = int(y1Index)
        self.x2Index = int(x2Index)
        self.y2Index = int(y2Index)

        if abs(self.x1Index-self.x2Index) + abs(self.y1Index-self.y2Index) <= 1:
            self.type = 0
        else:
            self.type = 1

        self.playerStatus = setting.STATUS.NOT_VALID
        if x1Index == x2Index and y1Index == y2Index:
            self.playerStatus = setting.STATUS.STAND
        elif x1Index == x2Index:
            self.playerStatus = setting.STATUS.HORIZONTAL 
        elif y1Index == y2Index:
            self.playerStatus = setting.STATUS.VERTICAL 
        else:
            self.playerStatus = setting.STATUS.NOT_VALID
        
        self.x1 = setting.INIT_X + x1Index*setting.STEP
        self.y1 = setting.INIT_Y + y1Index*setting.STEP
        self.x2 = setting.INIT_X + x2Index*setting.STEP
        self.y2 = setting.INIT_Y + y2Index*setting.STEP
    
    def draw(self):
        if self.type == 0:
            if self.isDraw1First == True:
                setting.DISPLAYSURF.blit(self.surface1, (self.x1, self.y1))
                setting.DISPLAYSURF.blit(self.surface1, (self.x2, self.y2))
            else:
                setting.DISPLAYSURF.blit(self.surface1, (self.x2, self.y2))
                setting.DISPLAYSURF.blit(self.surface1, (self.x1, self.y1))
        elif self.type == 1:
            setting.DISPLAYSURF.blit(self.surface1, (self.x1, self.y1))
            setting.DISPLAYSURF.blit(self.surface2, (self.x2, self.y2))
        else:
            setting.DISPLAYSURF.blit(self.surface2, (self.x1, self.y1))
            setting.DISPLAYSURF.blit(self.surface1, (self.x2, self.y2))
    def updateEndMoving(self):
        self.x1Index = int((self.x1 - setting.INIT_X)/setting.STEP)
        self.y1Index = int((self.y1 - setting.INIT_Y)/setting.STEP)
        self.x2Index = int((self.x2 - setting.INIT_X)/setting.STEP)
        self.y2Index = int((self.y2 - setting.INIT_Y)/setting.STEP)

        if abs(self.x1Index-self.x2Index) + abs(self.y1Index-self.y2Index) <= 1:
            self.type = 0

        if self.x1Index == self.x2Index and self.y1Index == self.y2Index:
            self.playerStatus = setting.STATUS.STAND
        elif self.x1Index == self.x2Index:
            self.playerStatus = setting.STATUS.HORIZONTAL 
        elif self.y1Index == self.y2Index:
            self.playerStatus = setting.STATUS.VERTICAL 
        else:
            self.playerStatus = setting.STATUS.NOT_VALID
        
        self.count += 1
        self.isEndMoving = True
    def split(self, pos1, pos2):
        self.x1Index = pos1[0]
        self.y1Index = pos1[1]
        self.x2Index = pos2[0]
        self.y2Index = pos2[1]

        if abs(self.x1Index-self.x2Index) + abs(self.y1Index-self.y2Index) <= 1:
            self.type = 0
        else:
            self.type = 1

        self.x1 = setting.INIT_X + self.x1Index*setting.STEP
        self.y1 = setting.INIT_Y + self.y1Index*setting.STEP
        self.x2 = setting.INIT_X + self.x2Index*setting.STEP
        self.y2 = setting.INIT_Y + self.y2Index*setting.STEP

    def moveLeft(self):
        if self.isCanMove == True: #Chi chay mot lan luc bat dau nhan nut di chuyen sang trai
            if self.x1Index<self.x2Index:
                self.isDraw1First = True
            elif self.x1Index>self.x2Index:
                self.isDraw1First = False
            self.isMoveLeft = True
            self.isCanMove = False

        if self.type == 0:
            if self.x1Index == self.x2Index:
                if self.y1Index != self.y2Index:
                    self.x1 -= setting.SPEED
                    self.x2 -= setting.SPEED
                else:
                    if self.isDraw1First == True:
                        self.x1 -= setting.SPEED
                        self.x2 -= 2*setting.SPEED
                    else:
                        self.x1 -= 2*setting.SPEED
                        self.x2 -= setting.SPEED
            elif self.x1Index < self.x2Index:
                self.x1 -= setting.SPEED
                self.x2 -= 2*setting.SPEED
            else:
                self.x1 -= 2*setting.SPEED
                self.x2 -= setting.SPEED
        elif self.type == 1:
            self.x1 -= setting.SPEED
        else: #self.type == 2:
            self.x2 -= setting.SPEED

        if self.x1 % setting.STEP == 0 and self.x2 % setting.STEP == 0: #Ket thuc di chuyen sang trai
            self.updateEndMoving()
            self.isMoveLeft = False
            self.isCanMove = True

    def moveRight(self):
        if self.isCanMove == True: #Chi chay mot lan luc bat dau nhan nut di chuyen sang phai
            if self.x1Index<self.x2Index:
                self.isDraw1First = False
            elif self.x1Index>self.x2Index:
                self.isDraw1First = True
            self.isMoveRight = True
            self.isCanMove = False

        if self.type == 0:
            if self.x1Index == self.x2Index:
                if self.y1Index != self.y2Index:
                    self.x1 += setting.SPEED
                    self.x2 += setting.SPEED
                else:
                    if self.isDraw1First == True:
                        self.x1 += setting.SPEED
                        self.x2 += 2*setting.SPEED
                    else:
                        self.x1 += 2*setting.SPEED
                        self.x2 += setting.SPEED
            elif self.x1Index < self.x2Index:
                self.x1 += 2*setting.SPEED
                self.x2 += setting.SPEED
            else:
                self.x1 += setting.SPEED
                self.x2 += 2*setting.SPEED
        elif self.type == 1:
            self.x1 += setting.SPEED
        else: #self.type == 2:
            self.x2 += setting.SPEED

        if self.x1 % setting.STEP == 0 and self.x2 % setting.STEP == 0: #Ket thuc di chuyen sang phai
            self.updateEndMoving()
            self.isMoveRight = False
            self.isCanMove = True

    def moveUp(self):
        if self.isCanMove == True: #Chi chay mot lan luc bat dau nhan nut di chuyen len tren
            if self.y1Index<self.y2Index:
                self.isDraw1First = True
            elif self.y1Index>self.y2Index:
                self.isDraw1First = False
            self.isMoveUp = True
            self.isCanMove = False

        if self.type == 0:
            if self.y1Index == self.y2Index:
                if self.x1Index != self.x2Index:
                    self.y1 -= setting.SPEED
                    self.y2 -= setting.SPEED
                else:
                    if self.isDraw1First == True:
                        self.y1 -= setting.SPEED
                        self.y2 -= 2*setting.SPEED
                    else:
                        self.y1 -= 2*setting.SPEED
                        self.y2 -= setting.SPEED
            elif self.y1Index < self.y2Index:
                self.y1 -= setting.SPEED
                self.y2 -= 2*setting.SPEED
            else:
                self.y1 -= 2*setting.SPEED
                self.y2 -= setting.SPEED
        elif self.type == 1:
            self.y1 -= setting.SPEED
        else: #self.type == 2:
            self.y2 -= setting.SPEED

        if self.y1 % setting.STEP == 0 and self.y2 % setting.STEP == 0: #Ket thuc di chuyen len tren
            self.updateEndMoving()
            self.isMoveUp = False
            self.isCanMove = True

    def moveDown(self):
        if self.isCanMove == True: #Chi chay mot lan luc bat dau nhan nut di chuyen xuong duoi
            if self.y1Index<self.y2Index:
                self.isDraw1First = False
            elif self.y1Index>self.y2Index:
                self.isDraw1First = True
            self.isMoveDown = True
            self.isCanMove = False

        if self.type == 0:
            if self.y1Index == self.y2Index:
                if self.x1Index != self.x2Index:
                    self.y1 += setting.SPEED
                    self.y2 += setting.SPEED
                else:
                    if self.isDraw1First == True:
                        self.y1 += setting.SPEED
                        self.y2 += 2*setting.SPEED
                    else:
                        self.y1 += 2*setting.SPEED
                        self.y2 += setting.SPEED
            elif self.y1Index < self.y2Index:
                self.y1 += 2*setting.SPEED
                self.y2 += setting.SPEED
            else:
                self.y1 += setting.SPEED
                self.y2 += 2*setting.SPEED
        elif self.type == 1:
            self.y1 += setting.SPEED
        else: #self.type == 2:
            self.y2 += setting.SPEED

        if self.y1 % setting.STEP == 0 and self.y2 % setting.STEP == 0: #Ket thuc di chuyen len tren
            self.updateEndMoving()
            self.isMoveDown = False
            self.isCanMove = True

    def update(self): # Hàm dùng để thay đổi vị trí xe
        if self.isMoveLeft == True:
            self.moveLeft()
        if self.isMoveRight == True:
            self.moveRight()
        if self.isMoveUp == True:
            self.moveUp()
        if self.isMoveDown == True:
            self.moveDown()