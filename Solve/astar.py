from Solve.fibonaciHeap import FibonacciHeap
from moveSimulation import move
from gridManager import grid
from Solve.hashTable import HashTable

class Position:
    def __init__(self, x1,  y1 ,x2,  y2, soft, hard, playerType):
        self.x1 = x1
        self.x2 = x2
        self.y1 = y1
        self.y2 = y2
        self.visited_pos = (x1, y1, x2, y2,soft, hard, playerType)
        self.rev_visited_pos= (x2, y2, x1, y1, soft, hard, playerType)
    def print_pos(self):
        print('x1=', self.x1, 'x2=', self.x2)
        print('y1=', self.y1, 'y2=', self.y2)
        print('')

def calcMahataan(pos, goalx, goaly):
    dis1 = abs( pos.x1  - goalx) + abs( pos.y1 - goaly)
    dis2 = abs( pos.x2  - goalx) + abs( pos.y2 - goaly)
    return dis1+dis2

def aStarSearch(pos, goalx, goaly, initalSoftButtonStatusString, initalHardButtonStatusString, playerType):
    # init state priority queue : (f, g, checkOrder, Position, actions, softStatus, hardStatus, playerType)
    tableSize = grid.height * grid.width 
    pri_q = FibonacciHeap()
    visited = HashTable( tableSize)
    checkOrder = 0

    # init start information
    pri_q.insert_node(( 0, 0, checkOrder, pos, [], initalSoftButtonStatusString, initalHardButtonStatusString, playerType))
    # add coordinates of initial position
    visited.add( pos.visited_pos)
    if pos.visited_pos != pos.rev_visited_pos:
        visited.add( pos.rev_visited_pos)

    # # main src
    while True:
        # check invalid
        if pri_q.isEmpty():
            print("No solution found\n")
            return ""
        
        # get best value
        (curF, curG, curcheckOrder, curPos, actions, currentSoftStatus, currentHardStatus, currentPlayerType) = pri_q.extract_min() 

        # render children in 4 directions
        moves = ['L','R','U','D', 'S']
        for m in moves:
            checkOrder += 1
            new_x1,new_y1,new_x2,new_y2,res, newSoftStatus,newHardStatus, newPlayerType = move(curPos.x1,curPos.y1,curPos.x2,curPos.y2,m, currentSoftStatus, currentHardStatus, currentPlayerType)

            # not valid
            if res == -1: continue

            # calculate for new children
            newPos = Position( new_x1, new_y1, new_x2, new_y2, newSoftStatus,newHardStatus, newPlayerType)
            newG = curG + 1
            newH = calcMahataan( newPos, goalx, goaly )
            newF = newG + newH

            # check pos is visited ?
            if visited.check(newPos.visited_pos) == 0:
                visited.add( newPos.visited_pos)
                if newPos.visited_pos != newPos.rev_visited_pos:
                    visited.add( newPos.rev_visited_pos)

                # check win
                if res == 1:
                    solution_str = ''.join(actions+[m])
                    return solution_str
                
                pri_q.insert_node((newF, newG, checkOrder, newPos, actions+[m], newSoftStatus, newHardStatus, newPlayerType))
                


