
import sys
# implement DOUBLE hash
class HashTable:
        def __init__(self, capacity, c = 11):
            self.capacity = capacity*capacity*1000
            self.table = [None]*capacity*capacity*1000
            self.c = c

        def check_size(self):
            print(sys.getsizeof(self.table))

        def add(self, item):
            index = hash(item)
            h1 = index % self.capacity
            h2 = 1 + index % ( self.capacity - 2)
            for i in range(self.capacity):
                cur_index = ( h1 + i * h2 * self.c ) % self.capacity
                if self.table[cur_index] == None:
                    self.table[cur_index] = item
                    return 1
            return -1
        
        def check(self,item):
            value = item
            index =hash( item )
            h1 = index % self.capacity
            h2 = 1 + index % ( self.capacity - 2)
            for i in range(self.capacity):
                cur_index = ( h1 + i * h2 * self.c ) % self.capacity
                if self.table[cur_index] == None:
                    return 0
                elif self.table[cur_index] == value:
                    return 1
            return 0
        
        def print_table(self):
            for i in range(self.capacity):
                print(self.table[i] , end = ' ')

