import moveSimulation
import random
import math
from gridManager import grid
from Solve.hashTable import HashTable


class Node:
    def __init__(self, x1, y1, x2, y2, res, SoftButn, HardButn, pType, prev=None):
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        self.SoftButn = SoftButn
        self.HardButn = HardButn
        self.pType = pType
        self.res = res
        self.parent = None
        self.children = []
        self.visits = 0
        self.score = 0
        self.uctScore = 0
        self.prev = prev
        self.failChild = 0

    def AppendChild(self, child, action):
        self.children.append(child)
        child.parent = self
        child.prev = action


class MonteCarloTreeSearch:
    # -----------------------------------------------------------------------#
    # Phu Hung dep trai o yeah <3
    # Description: Constructor.
    # Node 	  - Root node of the tree of class Node.
    # Verbose - True: Print details of search during execution.
    # 			False: Otherwise
    # -----------------------------------------------------------------------#
    def __init__(self, x1, y1, x2, y2, initSoftButn, initHardButn, pType, verbose=False):
        self.root = Node(x1, y1, x2, y2, 0, initSoftButn,
                         initHardButn, pType, 'Root')
        self.verbose = verbose

    # -----------------------------------------------------------------------#
    # Description: Selection.
    #       Implement Selection phase in MCTS
    # Start at root
    # Stop and return if found:
    #       + node with no child (leaf node, not yet explored child node)
    #       + unexpanded node (these child node have explored but have not yet visited/simulate)
    # Select child node base on highest UCT value(only when all child node have been visited/simulated)
    # -----------------------------------------------------------------------#
    def Selection(self):
        SelectedChild = self.root
        hasChild = True if SelectedChild.children else False
        while (hasChild):
            SelectedChild = self.SelectChild(SelectedChild)
            if not SelectedChild.children:
                hasChild = False
        if (self.verbose):
            print("Selected: ", SelectedChild.prev)
        return SelectedChild

    def SelectChild(self, node):
        # Found leaf node
        if not node.children and node.res != -1:
            return node
        # Find those exlored child nodes but not yet visit/simulate
        for child in node.children:
            if child.visits > 0:
                continue
            else:
                if (self.verbose):
                    print("Considered select unexpanded child: ", child.prev, "")
                return child
        best_score = float('-inf')
        # When all child node been visited/simulate at least once -> they'll have uct score -> take the highest score
        for child in node.children:
            score = child.uctScore
            if (self.verbose):
                print("Considered expanded child with utc score: ", score, "")
            if child.res != -1 and score > best_score:
                SelectedChild = child
                best_score = score
        return SelectedChild

    # -----------------------------------------------------------------------#
    # Description: Expansion.
    #       Implement Expansion phase in MCTS
    #  Incase:
    #        + unvisited node -> do nothing and return this node (to go on Simulate phase and simulate this node first :D )))
    #        + node not yet explored children -> explored all available children -> pick 1 node to return
    #  If explored the Win game node -> end search
    # -----------------------------------------------------------------------#

    def expansion(self, node, visited):
        if node.visits == 0 and node != self.root:
            return node
        if not node.children:
            actions = ["L", "R", "U", "D", "S"]
            for action in actions:
                new_x1, new_y1, new_x2, new_y2, res, newSoftStatus, newHardStatus, newPlayerType = moveSimulation.move(
                    node.x1, node.y1, node.x2, node.y2, action, node.SoftButn, node.HardButn, node.pType)
                if res == -1:
                    continue
                newPos = (new_x1, new_y1, new_x2, new_y2,
                          newSoftStatus, newHardStatus, newPlayerType)
                revPos = (new_x2, new_y2, new_x1, new_y1,
                          newSoftStatus, newHardStatus, newPlayerType)
                if visited.check(newPos) == 0 and visited.check(revPos) == 0:
                    if (self.verbose):
                        print("Expand ", newPos)
                    visited.add(newPos)
                    new_node = Node(new_x1, new_y1, new_x2,
                                    new_y2, res,
                                    newSoftStatus, newHardStatus, newPlayerType, action)
                    node.AppendChild(new_node, action)
                if (res == 1):
                    self.markSolutionNode(new_node)
                    return False
            if not node.children:
                node.res = -1
                return node
        returnnode = random.choice(node.children)
        return returnnode

    # -----------------------------------------------------------------------#
    # Description: Simulation
    #       Implement Simulation phase in MCTS
    #  Simulate with only available move(not die move) until reach the goal or run out of num_simulate
    # -----------------------------------------------------------------------#

    def simulation(self, node, num_simulate, visited):
        if (node.res == -1):
            return self.get_score(-1)
        state = (node.x1, node.y1, node.x2, node.y2, node.res,
                 node.SoftButn, node.HardButn, node.pType)
        actionSet = {"L", "R", "U", "D", "S"}
        availAction = actionSet
        for i in range(num_simulate):
            action = random.choice(tuple(availAction))
            stateTemp = tuple(moveSimulation.move(
                state[0], state[1], state[2], state[3], action, state[5], state[6], state[7]))
            if stateTemp[4] != -1:
                if visited.check((stateTemp[0], stateTemp[1], stateTemp[2], stateTemp[3], stateTemp[5], stateTemp[6], stateTemp[7])) == 0 and visited.check((stateTemp[2], stateTemp[3], stateTemp[0], stateTemp[1], stateTemp[5], stateTemp[6], stateTemp[7])) == 0:
                    state = stateTemp
                    avoidAction = ('L' and action == 'R') or ('R' and action == 'L') or (
                        'U' and action == 'D') or ('D' and action == 'U') or ('' and action == 'S')
                    availAction = actionSet - {avoidAction}
                else:
                    availAction -= {action}
            else:
                availAction -= {action}
            if (state[4] == 1):
                break

            if not len(availAction):
                return self.get_score(-1)
        if (self.verbose):
            print('Final simulation result :', state[4])
        return self.get_score(state[4])

    # -----------------------------------------------------------------------#
    # Description: Backpropagation
    #       Implement Backpropagation phase in MCTS
    #  Backtrack stack visit value, score value, re evaluate UCT score on node
    # -----------------------------------------------------------------------#

    def backpropagation(self, node, score):
        currNode = node

        currNode.visits += 1
        currNode.score += score
        currFlag = True if currNode.res == -1 else False

        while currNode.parent:
            currNode = currNode.parent
            if currFlag:
                currNode.failChild += 1
            if currNode.failChild == len(currNode.children):
                currNode.res = -1
                currFlag = True
            else:
                currFlag = False
            currNode.visits += 1
            currNode.score += score
        currNode = node
        self.EvalUCT(currNode)
        while currNode.parent:
            currNode = currNode.parent
            self.EvalUCT(currNode)

    # -----------------------------------------------------------------------#
    # Description: Support Functions
    #    + EvalUCT: caculate UCT value for node
    #    + MarkSolution: mark goal node with highest (inf) score
    # -----------------------------------------------------------------------#

    def EvalUCT(self, node):
        c = 1.4
        score = node.score
        nodeVisit = node.visits
        if (node.parent == None):
            parentVisit = node.visits
        else:
            parentVisit = node.parent.visits
        UCT = score/nodeVisit + c * \
            math.sqrt(math.log(parentVisit) / nodeVisit)
        node.uctScore = UCT
        return UCT

    def markSolutionNode(self, node):
        node.uctScore = float('inf')

    # -----------------------------------------------------------------------#
    # Description: search Functions
    #    mainly run the search process
    # -----------------------------------------------------------------------#

    def search(self, maxIter, num_simulate):
        solvedFlag = False
        print('============= Begin Monte Carlo Tree Search =============')
        tableSize = grid.height * grid.width
        visited = HashTable(tableSize)
        for i in range(maxIter):
            # print('This is the ', i, 'search loop')
            node = self.Selection()
            child = self.expansion(node, visited)
            if child:
                score = self.simulation(child, num_simulate, visited)
                self.backpropagation(child, score)
            else:
                solvedFlag = True
                break
        print('============= Finish Monte Carlo Tree Search =============')
        return solvedFlag

    # -----------------------------------------------------------------------#
    # Description: getPath Functions
    #    get solution path after search tree build
    # -----------------------------------------------------------------------#

    def getPath(self):
        path = []
        node = self.root
        while node.res != 1:
            best_score = float('-inf')
            for child in node.children:
                score = child.uctScore
                if child.res !=-1 and score > best_score:
                    SelectedChild = child
                    best_score = score
            path.append(SelectedChild.prev)
            node = SelectedChild
        return path

    # -----------------------------------------------------------------------#
    # Description: solve Functions
    #    trigger mcts search
    # -----------------------------------------------------------------------#

    def solve(self, maxIter, num_simulate):
        result = self.search(maxIter, num_simulate)
        if (self.verbose):
            self.PrintTree()
        if result:
            return self.getPath()
        return False

    def get_score(self, res):
        if res == -1:
            return -1
        if res == 0:
            return 1
        elif res == 1:
            return 2000

    # -----------------------------------------------------------------------#
    # Description: Print Tree Functions
    #    support sinh vien kiem tra cay mcts :D xuat ra file 'Tree.txt'
    # -----------------------------------------------------------------------#

    # print treeee
    def GetLevel(self, Node):
        Level = 0
        while (Node != self.root):
            Level += 1
            Node = Node.parent
        return Level

    def PrintTree(self):
        f = open('Tree.txt', 'w')
        Node = self.root
        self.PrintNode(f, Node, "", False)
        f.close()

    def PrintNode(self, file, Node, Indent, IsTerminal=False):
        file.write(Indent)
        if (IsTerminal):
            file.write("\-")
            Indent += "  "
        else:
            file.write("|-")
            Indent += "| "
        string = "Level: " + str(self.GetLevel(Node)) + ") (["
        string += str(Node.prev)
        string += "]," + " W: " + str(Node.score) + ", Visit: " + \
            str(Node.visits) + ", UCT: " + str(Node.uctScore) + \
            ("Fail" if Node.res == -1 else "")+") \n"
        file.write(string)
        for Child in Node.children:
            self.PrintNode(file, Child, Indent)

# -----------------------------------------------------------------------#
# Description: mctsSolver Functions
#    default parameter maxIter =50,000
#                      num_simulate = x grid + y grid
#    Use for: BTL_AI will call this function for trigger MCTS process
# -----------------------------------------------------------------------#

def mctsSolver(mtcsTree, maxIter, num_simulate):
    if not maxIter:
        maxIter = 50000
    if not num_simulate:
        num_simulate = grid.height + grid.width
    solutionPath = mtcsTree.solve(maxIter, num_simulate)
    if solutionPath:
        st = "".join(solutionPath)
        return st
    else:
        print('No solution found\n')
        return ""
