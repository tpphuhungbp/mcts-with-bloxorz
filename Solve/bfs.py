import pygame, sys, re, time, Grid, moveSimulation
from queue import Queue
from pygame.locals import *
from enum import Enum, unique, Flag
from gridManager import grid
from Solve.hashTable import HashTable

def bfs(init_x1,init_y1,init_x2,init_y2, initalSoftButtonStatusString, initalHardButtonStatusString,playerType):
    queues = Queue()
    tableSize = grid.height * grid.width 
    visited = HashTable( tableSize)
    queues.put((init_x1,init_y1,init_x2,init_y2,0,[], initalSoftButtonStatusString,initalHardButtonStatusString,playerType))
    visited.add((init_x1,init_y1,init_x2,init_y2, initalSoftButtonStatusString, initalHardButtonStatusString,playerType))
    while True:
        if queues.empty():
            print("No solution found\n")
            return ""
        (cur_x1,cur_y1,cur_x2,cur_y2,steps,actions, soft,hard,type) = queues.get()
        moves = ['L','R','U','D','S']
        for m in moves:
            new_x1,new_y1,new_x2,new_y2,res, new_softButtonStatusString, new_hardButtonStatusString,new_playerType = moveSimulation.move(cur_x1,cur_y1,cur_x2,cur_y2,m, soft, hard,type)
            if res == -1: continue
            if visited.check( (new_x1,new_y1,new_x2,new_y2, new_softButtonStatusString, new_hardButtonStatusString,new_playerType) ) == 0 :
                visited.add((new_x1,new_y1,new_x2,new_y2, new_softButtonStatusString, new_hardButtonStatusString,new_playerType))
                if res == 1:
                    solution_str = ''.join(actions+[m])
                    return solution_str
                if m!='S':
                    steps += 1
                queues.put((new_x1,new_y1,new_x2,new_y2,steps,actions+[m], new_softButtonStatusString, new_hardButtonStatusString,new_playerType))
