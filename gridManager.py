import pygame, sys, re, time, setting
from queue import Queue
from pygame.locals import *
from enum import Enum, unique, Flag

class Grid():
    def __init__(self):
        self.surfaceEmpty = pygame.image.load("Sprite/Empty.png")
        self.surfaceFloor = pygame.image.load("Sprite/floor.png")
        self.surfaceGoal = pygame.image.load("Sprite/Goal.png")
        self.surfaceCrackFloor = pygame.image.load("Sprite/CrackFloor.png")
        self.surfaceSorfSwitch = pygame.image.load("Sprite/SorfSwitch.png")
        self.surfaceHardSwitch = pygame.image.load("Sprite/HardSwitch.png")
        self.surfaceSplitSwitch = pygame.image.load("Sprite/SplitSwitch.png")

        self.goalX = 0
        self.goalY = 0

    def draw(self):
        for i in range(0,self.height):
            for j in range(0,self.width):
                if self.arr[i][j] == 0:
                    setting.DISPLAYSURF.blit(self.surfaceEmpty, (setting.INIT_X + j*setting.STEP, setting.INIT_Y + i*setting.STEP))
                elif self.arr[i][j] == 1:
                    setting.DISPLAYSURF.blit(self.surfaceFloor, (setting.INIT_X + j*setting.STEP, setting.INIT_Y + i*setting.STEP))
                elif self.arr[i][j] == 2:
                    setting.DISPLAYSURF.blit(self.surfaceGoal, (setting.INIT_X + j*setting.STEP, setting.INIT_Y + i*setting.STEP))
                    self.goalX = j
                    self.goalY = i
                elif self.arr[i][j] == 3:
                    setting.DISPLAYSURF.blit(self.surfaceCrackFloor, (setting.INIT_X + j*setting.STEP, setting.INIT_Y + i*setting.STEP))
                elif self.arr[i][j] == 4:
                    setting.DISPLAYSURF.blit(self.surfaceSorfSwitch, (setting.INIT_X + j*setting.STEP, setting.INIT_Y + i*setting.STEP))
                elif self.arr[i][j] == 5:
                    setting.DISPLAYSURF.blit(self.surfaceHardSwitch, (setting.INIT_X + j*setting.STEP, setting.INIT_Y + i*setting.STEP))
                elif self.arr[i][j] == 6:
                    setting.DISPLAYSURF.blit(self.surfaceSplitSwitch, (setting.INIT_X + j*setting.STEP, setting.INIT_Y + i*setting.STEP))

    def setData(self, width, height, arr):
        self.width = width
        self.height = height
        self.arr = arr
        for i in range( len(arr)):
            for j in range(len(arr[0])):
                if arr[i][j] == 2:
                    self.goalY = i
                    self.goalX = j

grid = Grid()
initalGrid = Grid()

